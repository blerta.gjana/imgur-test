import Axios from 'axios';

const API = Axios.create({
    baseURL: 'https://api.imgur.com/3/',
    headers: {
        Authorization: 'Client-ID d90084c9e92683b',
        'Content-Type': 'application/json'
    }
});

export default API;
