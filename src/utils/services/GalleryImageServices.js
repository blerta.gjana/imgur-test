import API from '../../utils/plugins/API';

const GalleryImageService = {
    allImage: (section, sort, window, page, showViral, showMature, albumPrevious) => {
        return API(
            `gallery/${section}/${sort}/${window}/${page}?showViral=${showViral}&mature=${showMature}&albumPrevious=${albumPrevious}`
        );
    }
};
export default GalleryImageService;
