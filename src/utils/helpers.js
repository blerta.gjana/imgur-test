export const formatDate = (date) => {
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dev'];
    return `${date.getHours()}:${date.getMinutes()}, ${
        monthNames[date.getMonth() - 1]
    } ${date.getDate()}`;
};

export const formatLink = (coverImg, linkImg) => {
    // eslint-disable-next-line no-undef
    let src = '';
    const linkMp4 = linkImg.substring(linkImg.length - 4);
    if (coverImg) {
        src = `//imgur.com/${coverImg}.jpg`;
    } else if (linkMp4 === '.mp4') {
        const removeMp4 = linkImg.replace('.mp4', '.jpg');
        src = `${removeMp4}`;
    } else if (linkMp4 === '.gif') {
        const removeGif = linkImg.replace('.gif', '.jpg');
        src = `${removeGif}`;
    } else if (linkMp4 === '.png') {
        const removePng = linkImg.replace('.png', '.jpg');
        src = `${removePng}`;
    }
    return src;
};