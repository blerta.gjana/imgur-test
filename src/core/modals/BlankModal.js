import React from 'react';
import { FaTimes } from 'react-icons/fa';

const BlankModal = ({ setOpenModal, children, maxW = 'lg', bg, width }) => {
    return (
        <div
            style={{ zIndex: 9999 }}
            className="fixed inset-0 overflow-y-auto"
            aria-labelledby="modal-title"
            role="dialog"
            aria-modal="true">
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                <div
                    className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"
                    aria-hidden="true"
                    onClick={() => {
                        setOpenModal(false);
                    }}
                />
                <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
          &#8203;
        </span>
                <div
                    className={`inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-${maxW} ${width}`}>
                    <div className={`bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4 ${bg}`}>
                        {' '}
                        <div
                            className="grid justify-items-end text-2xl cursor-pointer"
                            onClick={() => {
                                setOpenModal(false);
                            }}>
                            <FaTimes />
                        </div>
                        {children}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BlankModal;
