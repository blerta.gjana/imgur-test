import React, { useCallback, useEffect, useState } from 'react';
import { formatLink } from '../utils/helpers';
import { sectionArray, sortArray, windowArray } from '../constants/FilterImageOptions';
import GalleryImageService from '../utils/services/GalleryImageServices';
import Select from 'react-select';
import ImageDetails from '../components/Images/ImageDetails';
import Pagination from '@mui/material/Pagination';

const ImageGallery = () => {
    const [loading, setLoading] = useState(true);
    const [allImages, setAllImages] = useState([]);
    const [section, setSection] = useState(sectionArray[0]);
    const [sort, setSort] = useState(sortArray[0]);
    const [window, setWindow] = useState(windowArray[0]);
    const [pageImage, setPageImage] = useState(0);
    const [showViral, setShowViral] = useState(true);
    const [mature, setMature] = useState(false);
    const [albumPrevious, setAlbumPrevious] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    const [modalInfoIsOpen, setModalInfoIsOpen] = useState(false);
    const [selectedImage, setSelectedIMage] = useState(null);

    const customStyles = {
        control: (base) => ({
            ...base,
            height: 48,
            width: 175
        })
    };

    const handleChangeSection = (e) => {
        setSection(e);
    };

    const handleChangeSort = (e) => {
        setSort(e);
    };

    const handleChangeWindow = (e) => {
        setWindow(e);
    };

    const getImage = useCallback(() => {
        setLoading(true);
        GalleryImageService.allImage(
            section.value,
            sort.value,
            window.value,
            pageImage,
            showViral,
            mature,
            albumPrevious
        )
            .then((response) => {
                setAllImages(response.data.data);
            });
    }, [section.value, sort.value, window.value, pageImage, showViral, mature, albumPrevious]);

    useEffect(() => {
        getImage();
    }, [getImage]);

    return (
        <div className="flex flex-col container mx-auto">
            <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div className="py-4 inline-block min-w-full sm:px-6 md:px-4 lg:px-8">
                    <div className="lg:flex mb-6 hidden">
                        <Select
                            className="mr-2 w-48"
                            placeholder="All Sections"
                            options={sectionArray}
                            styles={customStyles}
                            value={section}
                            onChange={handleChangeSection}
                            getOptionLabel={(e) => (
                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    <span style={{ marginLeft: 5 }}>{e.text}</span>
                                </div>
                            )}
                        />
                        <Select
                            className="mr-2 w-48"
                            placeholder="All Sorts"
                            options={sortArray}
                            styles={customStyles}
                            value={sort}
                            onChange={handleChangeSort}
                            getOptionLabel={(e) => (
                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    <span style={{ marginLeft: 5 }}>{e.text}</span>
                                </div>
                            )}
                        />
                        <Select
                            className="mr-2 w-48"
                            placeholder="Window"
                            options={windowArray}
                            styles={customStyles}
                            value={window}
                            onChange={handleChangeWindow}
                            getOptionLabel={(e) => (
                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    <span style={{ marginLeft: 5 }}>{e.text}</span>
                                </div>
                            )}
                        />
                    </div>
                    <div className="overflow-hidden">
                        <div className="grid grid-cols-3 gap-7 text-center ">
                            {allImages.slice(0, 15).map((all) => (
                                <div className="shadow-lg">
                                    <div key={all.id} className="p-4">
                                        {/* eslint-disable-next-line jsx-a11y/alt-text,jsx-a11y/img-redundant-alt,jsx-a11y/no-noninteractive-element-interactions */}
                                        <img
                                            src={formatLink(all.cover, all.link)}
                                            className="max-h-52 cursor-pointer mx-auto"
                                            onClick={() => {
                                                setSelectedIMage(all);
                                                setOpenModal(true);
                                            }}
                                            alt="text"
                                        />
                                        <div className="mt-4 pt-3 border-t-2 border-purple-800">{all.title}</div>
                                        {openModal && (
                                            <ImageDetails imageInfo={selectedImage} openModal={setOpenModal} />
                                        )}
                                    </div>
                                </div>
                            ))}
                        </div>
                        <div className="flex float-right mt-8">
                            {' '}
                            Click to see more images in another pages
                            <Pagination
                                count={(e, value) => setPageImage(value)}
                                onChange={(e, value) => setPageImage(value)}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ImageGallery;
