import React from 'react';
import BlankModal from '../../core/modals/BlankModal';
import { formatLink, formatDate } from '../../utils/helpers';
import { FaArrowDown, FaArrowUp, FaComment, FaEye } from 'react-icons/fa';

const ImageDetails = ({ imageInfo, openModal }) => {

    return (
        <div>
            <BlankModal setOpenModal={openModal} bg="bg-gray-50" width="w-160">
                <div className="mt-3">
                    <div className="overflow-hidden">
                        <div>
                            <img
                                src={formatLink(imageInfo.cover, imageInfo.link)}
                                className="mx-auto"
                                alt="Image "
                            />
                        </div>
                        <div className="flex mt-5 text-center">
                            <div className="flex-1">Posted by: {imageInfo.account_url}</div>
                            <div className="flex-1">
                                Posted at: {formatDate(new Date(imageInfo.datetime * 1000))}
                            </div>
                        </div>
                        <div className="flex mt-5">
                            <div className="flex flex-1 items-center justify-center">
                                <FaEye className="mr-2" /> {imageInfo.views}
                            </div>
                            <div className="flex flex-1 items-center justify-center">
                                <FaComment className="mr-2" /> {imageInfo.comment_count}
                            </div>
                            <div className="flex flex-1 items-center justify-center">
                                <FaArrowUp className="mr-2" /> {imageInfo.ups}
                            </div>
                            <div className="flex flex-1 items-center justify-center">
                                <FaArrowDown className="mr-2" /> {imageInfo.downs}
                            </div>
                        </div>
                    </div>
                </div>
            </BlankModal>
        </div>
    );
};

export default ImageDetails;
