export const sectionArray = [
    {
        id: '90a59d6d-4224-41ac-912f-c0453805abed',
        value: 'hot',
        text: <div className=" ml-1 text-sm">HOT</div>
    },
    {
        id: '8d812f6f-916a-4a54-b9a9-2fd4682eafab',
        value: 'top',
        text: <div className=" ml-1 text-sm">TOP</div>
    },
    {
        id: '4b6f9235-8477-4c7b-863f-7e85a085ce63',
        value: 'user',
        text: <div className=" ml-1 text-sm">USER</div>
    },
    {
        id: '90a59d6d-4224-41ac-912f-c0453805abed',
        value: null,
        text: <div className=" ml-1 text-sm">ALL</div>
    }
];

export const sortArray = [
    {
        id: '90a59d6d-4224-41ac-912f-c0453805abed',
        value: 'viral',
        text: <div className=" ml-1 text-sm">VIRAL</div>
    },
    {
        id: '8d812f6f-916a-4a54-b9a9-2fd4682eafab',
        value: 'top',
        text: <div className=" ml-1 text-sm">TOP</div>
    },
    {
        id: '8d812f6f-916a-4a54-b9a9-2fd4682eafab',
        value: 'time',
        text: <div className=" ml-1 text-sm">TIME</div>
    },
    {
        id: '8d812f6f-916a-4a54-b9a9-2fd4682eafab',
        value: 'rising',
        text: <div className=" ml-1 text-sm">RISING</div>
    },
    {
        id: '90a59d6d-4224-41ac-912f-c0453805abed',
        value: null,
        text: <div className=" ml-1 text-sm">ALL</div>
    }
];

export const windowArray = [
    {
        id: '90a59d6d-4224-41ac-912f-c0453805abed',
        value: 'day',
        text: <div className=" ml-1 text-sm">DAY</div>
    },
    {
        id: '8d812f6f-916a-4a54-b9a9-2fd4682eafab',
        value: 'week',
        text: <div className=" ml-1 text-sm">WEEK</div>
    },
    {
        id: '8d812f6f-916a-4a54-b9a9-2fd4682eafab',
        value: 'month',
        text: <div className="ml-1 text-sm">MONTH</div>
    },
    {
        id: '8d812f6f-916a-4a54-b9a9-2fd4682eafab',
        value: 'year',
        text: <div className=" ml-1 text-sm">YEAR</div>
    },
    {
        id: '90a59d6d-4224-41ac-912f-c0453805abed',
        value: null,
        text: <div className=" ml-1 text-sm">ALL</div>
    }
];
